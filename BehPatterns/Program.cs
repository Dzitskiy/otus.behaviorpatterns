﻿using System;

namespace BehPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            // var iterator = new Iterator();
            // iterator.Iterate();

            var visitor = new Visitor.Visitor();
            visitor.Visit();

            //var newsPublisher = new NewsPublisher();
            //newsPublisher.AddNews("Первая новость!");
            //var person = new Person(newsPublisher);
            //newsPublisher.AddNews("Вторая новость!");

            var strategy = new BaseStrategy();
            WithdrawService service = new WithdrawService(strategy);
            service.ValidateAmount(100);

            Console.ReadKey();
        }
    }
}
